To compile:
```
cmake -Bbuild -Ssource -DATLAS_PACKAGE_FILTER_FILE=extern_filters.txt
source build/${AnalysisBase_PLATFORM}/setup.sh
cmake --build build
cmake -Bbuild -Ssource -UATLAS_PACKAGE_FILTER_FILE
source build/${AnalysisBase_PLATFORM}/setup.sh
cmake --build build
```
